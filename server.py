#! /usr/local/bin/python3

from http.server import HTTPServer, CGIHTTPRequestHandler

httpd = HTTPServer(('', 8080), CGIHTTPRequestHandler)
print("Server Fired up")
httpd.serve_forever()
