#! /usr/local/bin/python3

import cgi
import cgitb

cgitb.enable()
print("Content-type: text/html \n\n")
print("""
<h2>Add your a site to engine</h2>

<form method="get" action="add_site.py">
	<legend>Enter a site url:</legend>
	<input type="text" name="site_url">
	<input type="submit" name="submit">
</form>
""")