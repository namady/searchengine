#! /usr/local/bin/python3

import cgi
import cgitb
import sqlite3
from crawler import *

def result_tmp(result_item, t):
    return """
        <p>
            <strong><a href="%s">%s</a></strong><br>
            %s<br>
            %s
        </p>
    """ % (result_item.link,t,result_item.link,result_item.preview)

cgitb.enable()
form = cgi.FieldStorage()
term = str(form['search_query'].value)
print("Content-type: text/html \n\n")
print("<h1>Search Processed</h1>")

search_result = search(term)

msg = """
    <p>
         you search for <b>%s</b>
""" % term

if len(search_result) == 0:
    msg += "did not match any result.</p>"
else:
    msg += ("return %s results.</p>" % len(search_result))
    
process_result = "<ul>"

for result in search_result:
    process_result += "<li>%s</li>" % result_tmp(result, term)

process_result += "</ul>"

print(process_result)