#! /usr/local/bin/python3

import re
import pickle
from html2text import html2text
import urllib.request

class SearchItem:
    def __init__(self, l, p):
        self.link = l
        self.preview = p
    def __repr__(self):
        return str(self.preview)
        

def start_search_engine():
    engine_words = dict()
    engine_urls = dict()
    write_engine(engine_words, engine_urls)

def search(terms):
    engine_words, engine_urls = read_engine()
    terms = set(terms.lower().split())
    search_result = []
    urls = []

    for each_term in terms:
        if each_term in engine_words:
            term_urls = engine_words.get(each_term)
            urls.extend(term_urls)
    for url in urls:
        if url in engine_urls:
            search_result.append(SearchItem(url, engine_urls.get(url)))
    return search_result

def save_to_searchengine(url):
    engine_words, engine_urls = read_engine()
    if engine_urls.get(url):
        return
    content = get_page(url)
    content = content.lower()
    content = re.findall(re.compile('\w+'), content)
    preview = ""
    for word in content:
        preview += word + " "
        if len(preview) >= 100:
            break
    engine_urls[url] = preview
    sanitized_content = set(content)
    for word in sanitized_content:
        tmp_list = engine_words.get(word)
        if tmp_list != None:
        	tmp_list.append(url)
        	engine_words[word] = tmp_list
        else:
        	engine_words[word] = [url]
    write_engine(engine_words, engine_urls)

def get_page(url):
    try:
        with urllib.request.urlopen(url) as response:
            contentx = str(response.read())
            content = html2text(contentx)
            return content
    except Exception as err:
    	print(str(err))
    	return "Error opening page" + str(err)

def get_all_links(page):
    links = []
    page_content = get_page(page)
    start = page_content.find('<a href=')
    while start is not -1:
        end = page_content.find('\"', start+9)
        links.append(page_content[start+9:end])
        page_content = page_content[end:]
        start = page_content.find('<a href=')
    return links
        
def get_all_links2(page):
    page = get_page(page)
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', page)
    return urls   

def crawl_pages(page):
    links_to_crawl = set(get_all_links2(page))
    crawled = set()
    while len(links_to_crawl) > 0 and len(crawled) < 20:
        link = links_to_crawl.pop()
        crawled.add(link)
        for each_link in get_all_links2(link):
            if each_link not in crawled:
                links_to_crawl.add(each_link)
    return crawled

def read_engine():
    engine_words = dict()
    engine_urls = dict()
    try:
        with open('engine_words.pickle', 'rb') as ew, open('engine_urls.pickle', 'rb') as eu:
            engine_words = dict(pickle.load(ew))
            engine_urls = dict(pickle.load(eu))
    except IOError as ioerr:
        print('Error opening file' + str(ioerr))
        return None, None
    return engine_words, engine_urls

def write_engine(engine_words, engine_urls):
    try:
        with open('engine_words.pickle', 'wb') as ew, open('engine_urls.pickle', 'wb') as eu:
            pickle.dump(engine_words, ew)
            pickle.dump(engine_urls, eu)
    except IOError as ioerr:
        print('Error saving data ' + str(ioerr))

#start_search_engine()
#print(search('ceo namady america http'))
#save_to_searchengine("http://www.namady.com")
#print(get_page('http://www.cnn.com'))